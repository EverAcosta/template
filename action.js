(() => {
let mascota = document.getElementById("mascota")
let boton = document.getElementById("boton")
mascota.addEventListener("click", (e) => {
    e.preventDefault()
    if(window.innerWidth<window.innerHeight){
        /*if (mascota.classList.contains('shake')) mascota.classList.remove('shake')
        mascota.classList.add('shake')*/
        animateCSS("mascota","shake")
    }
})
})
()
function animateCSS(element, animationName, callback) {
    const node = document.getElementById(element)
    node.src="/mascotaojoscerrados.png"
    node.classList.add('animated', animationName)
 
    function handleAnimationEnd() {
        node.src="/mascota.png"
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)
        if (typeof callback === 'function') callback()
    }
 
    node.addEventListener('animationend', handleAnimationEnd)
}